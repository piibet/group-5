﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Equipment_registry.Models.AdminViewModels;
using WebApplication2.Data;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Equipment_registry.Controllers
{
    //Authorize = admin, vain admin roolin omaavat pääsevät tänne
    [Authorize(Roles = "Admin, User")]
    // User pääsee myös, mutta toivon mukaan näkee vain itseensä liitetyt sopimukset




    public class ContractsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ContractsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Contracts

        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> Index()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier); // Tämän pitäisi tuottaa userId jota käytetään hakuun

            int customerId = 2; // Tämä pitäisi korvata oikealla arvolla

            var applicationDbContext = _context.Contract.Include(c => c.Customer).Where(i => i.CustomerId == customerId);

            return View(await applicationDbContext.ToListAsync());

        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Admin()
        {

            var applicationDbContext = _context.Contract.Include(c => c.Customer);
            
            return View(await applicationDbContext.ToListAsync());
        }


        // GET: Contracts/Details/5
        [Authorize(Roles = "Admin")]

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract
                .Include(c => c.Customer)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // GET: Contracts/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            var Customers = _context.Customer;
            //var userids = Customers.Select(c => c.UserName).ToList();
            //var users = _context.Users.Where(u => userids.Contains(u.UserName)); // TODO väärä string to int

            //var list = Customers.Select(s => new { Id = s.Id, UserName = s.UserName });

            ViewData["CustomerId"] = new SelectList(Customers, "Id", "UserName"); // eli AspnetUSe
            return View();
        }

        // POST: Contracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ContractType,BillingInterval,Price,InsuranceCompany,BeginDate,EndDate,Duration,CustomerId,AdminId")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contract);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerId"] = new SelectList(_context.Customer, "Id", "Id", contract.CustomerId);
            return View(contract);
        }

        // GET: Contracts/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract.SingleOrDefaultAsync(m => m.Id == id);
            if (contract == null)
            {
                return NotFound();
            }
            ViewData["CustomerId"] = new SelectList(_context.Customer, "Id", "Id", contract.CustomerId);
            return View(contract);
        }

        // POST: Contracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ContractType,BillingInterval,Price,InsuranceCompany,BeginDate,EndDate,Duration,CustomerId,AdminId")] Contract contract)
        {
            if (id != contract.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contract);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContractExists(contract.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerId"] = new SelectList(_context.Customer, "Id", "Id", contract.CustomerId);
            return View(contract);
        }

        // GET: Contracts/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract
                .Include(c => c.Customer)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // POST: Contracts/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contract = await _context.Contract.SingleOrDefaultAsync(m => m.Id == id);
            _context.Contract.Remove(contract);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContractExists(int id)
        {
            return _context.Contract.Any(e => e.Id == id);
        }
    }
}
