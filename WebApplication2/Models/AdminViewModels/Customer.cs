﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace Equipment_registry.Models.AdminViewModels
{
    public class Customer
    {
        public int Id { get; set; }
        public string ClientType { get; set; }
        public string BillingAddressLine1 { get; set; }
        public string BillingAddressLine2 { get; set; }
        public string BillingAddressLine3 { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string DefaultBillingAddress { get; set; }
        public string IBAN { get; set; }
        public string BankName { get; set; }
        public int PhoneNumber { get; set; }


        //Linkitys spostiin user
        [MaxLength(256)]
        public string UserName { get; set; }
        /*
        public ApplicationUser ApplicationUser { get; set; }  // Objekti referenssi

        public string CustomerName
        {
            get { return ApplicationUser.UserName; } // Tää

        }
        */
        
        //Monta sopimusta
        public List<Contract> Contracts { get; set; }

    }
}
