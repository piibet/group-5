﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Equipment_registry.Models.AdminViewModels
{
    public class Equipment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }
        public int LocationPostalCode { get; set; }
        public string CityName { get; set; }
        public string LocationContact { get; set; }
        public int LocationContactPhone { get; set; }
        public string EquipmentType { get; set; }
        public int EquipmentSerialNumber { get; set; }
        public string LocationDepartment { get; set; }
        public string LocationComment { get; set; }
        public string LastMaintenance { get; set; }
        public string InternalComment { get; set; }
        public string Important { get; set; }

        public int ContractId { get; set; }
        public Contract Contract { get; set; }  // Objekti referenssi


    }
}
