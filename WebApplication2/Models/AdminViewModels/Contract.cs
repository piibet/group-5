﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace Equipment_registry.Models.AdminViewModels
{
    public class Contract
    {
        public int Id { get; set; }
        public string ContractType { get; set; }
        public int BillingInterval { get; set; }
        public int Price { get; set; }
        public string InsuranceCompany { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Duration { get; set; }

        //Linkitys asikkaan
        public Customer Customer { get; set; }
        public int CustomerId { get; set; } //FK

        //Laite - Lista
        public List<Equipment> Equipment { get; set; }

        //Admin - Linkitys spostiin user
        public int AdminId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }  // Objekti referenssi

    }
}
