﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApplication2.Data.Migrations
{
    public partial class DatabaseRelations1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContractId",
                table: "Equipment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AdminId",
                table: "Contract",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Contract",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    BillingAddressLine1 = table.Column<string>(nullable: true),
                    BillingAddressLine2 = table.Column<string>(nullable: true),
                    BillingAddressLine3 = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    ClientType = table.Column<string>(nullable: true),
                    DefaultBillingAddress = table.Column<string>(nullable: true),
                    IBAN = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<int>(nullable: false),
                    PostalCode = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_ContractId",
                table: "Equipment",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_ApplicationUserId",
                table: "Contract",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_CustomerId",
                table: "Contract",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_ApplicationUserId",
                table: "Customer",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_AspNetUsers_ApplicationUserId",
                table: "Contract",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Customer_CustomerId",
                table: "Contract",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipment_Contract_ContractId",
                table: "Equipment",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_AspNetUsers_ApplicationUserId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Customer_CustomerId",
                table: "Contract");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipment_Contract_ContractId",
                table: "Equipment");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_Equipment_ContractId",
                table: "Equipment");

            migrationBuilder.DropIndex(
                name: "IX_Contract_ApplicationUserId",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_CustomerId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ContractId",
                table: "Equipment");

            migrationBuilder.DropColumn(
                name: "AdminId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Contract");
        }
    }
}
