﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Equipment_registry.Models.AdminViewModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Models;

namespace WebApplication2.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<Contract> Contract { get; set; }
        public DbSet<Customer> Customer { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
           /* builder.Entity<Customer>()
            .HasOne(p => p.ApplicationUser)
            .WithOne(a => a.Customer)
            .HasForeignKey<ApplicationUser>(b => b.CustomerId)
            ;
            */
            builder.Entity<Customer>()
            .HasMany(p => p.Contracts)
            ;

            builder.Entity<Contract>()
            .HasOne(p => p.ApplicationUser)
            ;

            builder.Entity<Contract>()
            .HasMany(p => p.Equipment)
            ;

            builder.Entity<Contract>()
            .HasOne(p => p.Customer)
            ;

            builder.Entity<Equipment>()
            .HasOne(p => p.Contract)
            ;


            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
